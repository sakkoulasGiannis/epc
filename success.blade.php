@extends('epc.layouts.app')

@section("content")

   <div class="container mt-5">
       <div class="row">


          <div class="col-md-12">
              <h1>{{__("frontend.success_checkout")}}</h1>
                <p>{{__("frontend.ordernum_is")}} {{$order->order_num}}</p>
                    
                <p>{{__("frontend.success_message")}}</p>
          </div>

          <div class="col-md-12">
            @if(isset($info))
            {!!$info!!}
            @endif
          </div>
          

       </div>
   </div>

    @endsection
