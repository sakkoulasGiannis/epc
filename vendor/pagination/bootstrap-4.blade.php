@if ($paginator->hasPages())
    <ul class="flat-pagination style1">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="prev">
                <a href="#" title="">
                    <img src="{{asset('views/epc/assets/images/icons/left-1.png')}}" alt="">
                </a>
            </li>
        @else
        
            <li class="prev">
                <a href="{{ $paginator->previousPageUrl() }}" title="">
                    <img src="{{asset('views/epc/assets/images/icons/left-1.png')}}" alt="">
                </a>
            </li>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class=" disabled"><span class="waves-effect">{{ $element }}</span></li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="active">
                            <a class="waves-effect" href="">{{ $page }}</a>
                        </li>
                    @else
                        <li class=""><a class="waves-effect" href="{{ $url }}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
        <li class="next">
            <a href="{{ $paginator->nextPageUrl() }}" title="">
                <img src="{{asset('views/epc/assets/images/icons/right-1.png')}}" alt="">
            </a>
        @else
        <li class="next">
            <a href="#" title="">
                <img src="{{asset('views/epc/assets/images/icons/right-1.png')}}" alt="">
            </a>
        </li>
        @endif
    </ul>
@endif
