@extends('epc.layouts.app')
@section("content")
    @section('title') {{$product->name}} @stop
 
    <section class="flat-product-detail">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<div class="flexslider">
							<ul class="slides">
                                    @if($product->getMedia("product") && count($product->getMedia("product")))
                                    @foreach($product->getMedia("product") as $var => $image)
                                        <li data-thumb="{{$image->getUrl()}}">
                                            <a href='#' id="zoom" class='zoom'><img src="{{$image->getUrl()}}" alt='' width='400' height='300' /></a>
                                        </li>
                                    @endforeach
                                @endif
							   
							</ul><!-- /.slides -->
						</div><!-- /.flexslider -->
					</div><!-- /.col-md-6 -->
					<div class="col-md-6">
						<div class="product-detail style4">
							<div class="header-detail">
                                    <h1 class="name">{{$product->name}}</h1>
								<div class="category">
									Smart Watches
								</div>
								<div class="reviewed">
									<div class="review">
										 	 
									</div>
									<div class="status-product">
										Availablity <span>In stock</span>
									</div>
								</div><!-- /.reviewed -->
							</div><!-- /.header-detail -->
							<div class="content-detail">
								<div class="price">
                                        @include('epc.elements.price')  
								</div>
								<div class="info-text">
                                        {!! $product->description !!}
								</div>
								<div class="product-id">
                                        SKU: <span class="id">{{ $product->sku}}</span>
								</div>
							</div><!-- /.content-detail -->
							<div class="footer-detail">
                                @if(count($product->sellable) > 0 ) {!! Form::open(['route' => ['cart.add', $product->slug], 'method' => 'post', "class"=>"row"]) !!} 
                                    {!! Form::hidden('quantity', 1, ['class' => 'form-control']) !!}
                                        <div class="col">
                                            <label>Επιλογή </label>
                                            <div class="form-group  ">
                                                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                    @foreach($apothiki as $inv)
                                                        @if($inv->id)
                                                            <label class="btn btn-secondary ">
                                                                <input type="radio" name="associated" value="{{$inv->id}}" id="option1" autocomplete="off"
                                                                    required>
                                                                {{$inv->value->title}}
                                                            </label>
                                                        @endif
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="product box-cart style2">
                                                <button class="mt-4 btn-product btn btn-add-cart"><img src="{{asset('views/epc/assets/images/icons/add-cart.png')}}" alt=""> <small>ΠΡΟΣΘΗΚΗ</small></button>
                                            </div>
                                        </div>
                                    {!! Form::close() !!} 
                                @else
                                    <p style="color: ;">Εκτός αποθέματoς</p>
                                @endif()

							 
							 
								<div class="social-single">
									<span>SHARE</span>
									<ul class="social-list style2">
										<li>
											<a href="#" title="">
												<i class="fa fa-facebook" aria-hidden="true"></i>
											</a>
										</li>

									</ul>
								</div><!-- /.social-single -->
							</div><!-- /.footer-detail -->
						</div><!-- /.product-detail style4 -->
					</div><!-- /.col-md-6 -->
				</div><!-- /.row -->
			</div><!-- /.container -->
		</section><!-- /.flat-product-detail -->

		<section class="flat-product-content">
			<ul class="product-detail-bar">
				<li class="active">Description</li>
			 
			</ul><!-- /.product-detail-bar -->
			<div class="container">
				<div class="row">
                    <div class='col-md-12'>
                        <div class="tecnical-specs">
                    
                                <table>
                                    <tbody>
                                            @foreach($product->attributes as $attribute)
                                            <tr>
                                                <td scope="row">{{$attribute['field_name']}}</td>
                                                <td>{{$attribute['title']}}</td>
                                            </tr>
                                            @endforeach
                                        
                                    </tbody>
                                </table>
                            </div><!-- /.tecnical-specs -->
                        </div>
					 
				</div><!-- /.row -->
				<div class="row">
					<div class="col-md-12">
						<div class="tecnical-specs">
							<h4 class="name">
								Warch 42 mm Smart Watches
							</h4>
							<table>
								<tbody>
									<tr>
										<td>Height</td>
										<td>38.6mm</td>
									</tr>
									<tr>
										<td>Meterial</td>
										<td>Stainless Stee</td>
									</tr>
									<tr>
										<td>Case</td>
										<td>40g</td>
									</tr>
									<tr>
										<td>Color</td>
										<td>blue, gray, green, light blue, lime, purple, red, yellow</td>
									</tr>
									<tr>
										<td>Depth</td>
										<td>10.5mm</td>
									</tr>
									<tr>
										<td>Width</td>
										<td>33.3mm</td>
									</tr>
									<tr>
										<td>Size</td>
										<td>Large, Medium, Small</td>
									</tr>
								</tbody>
							</table>
						</div><!-- /.tecnical-specs -->
					</div><!-- /.col-md-12 -->
				</div><!-- /.row -->
				<div class="row">
					<div class="col-md-6">
						<div class="rating">
							<div class="title">
								Based on 3 reviews
							</div>
							<div class="score">
								<div class="average-score">
									<p class="numb">4.3</p>
									<p class="text">Average score</p>
								</div>
								<div class="queue">
									<i class="fa fa-star" aria-hidden="true"></i>
									<i class="fa fa-star" aria-hidden="true"></i>
									<i class="fa fa-star" aria-hidden="true"></i>
									<i class="fa fa-star" aria-hidden="true"></i>
									<i class="fa fa-star" aria-hidden="true"></i>
								</div>
							</div>
							<ul class="queue-box">
								<li class="five-star">
									<span>
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
									</span>
									<span class="numb-star">3</span>
								</li>
								<li class="four-star">
									<span>
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
									</span>
									<span class="numb-star">4</span>
								</li>
								<li class="three-star">
									<span>
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
									</span>
									<span class="numb-star">3</span>
								</li>
								<li class="two-star">
									<span>
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
									</span>
									<span class="numb-star">2</span>
								</li>
								<li class="one-star">
									<span>
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
									</span>
									<span class="numb-star">0</span>
								</li>
							</ul>
						</div><!-- /.rating -->
					</div><!-- /.col-md-6 -->
					<div class="col-md-6">
						<div class="form-review">
							<div class="title">
								Add a review 
							</div>
							<div class="your-rating queue">
								<span>Your Rating</span>
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star" aria-hidden="true"></i>
							</div>
							<form action="#" method="get" accept-charset="utf-8">
								<div class="review-form-name">
									<input type="text" name="name-author" value="" placeholder="Name">
								</div>
								<div class="review-form-email">
									<input type="text" name="email-author" value="" placeholder="Email">
								</div>
								<div class="review-form-comment">
									<textarea name="review-text" placeholder="Your Name"></textarea>
								</div>
								<div class="btn-submit">
									<button type="submit">Add Review</button>
								</div>
							</form>
						</div><!-- /.form-review -->
					</div><!-- /.col-md-6 -->
					<div class="col-md-12">
						<ul class="review-list">
							<li>
								<div class="review-metadata">
									<div class="name">
										Ali Tufan : <span>April 3, 2016</span>
									</div>
									<div class="queue">
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
									</div>
								</div><!-- /.review-metadata -->
								<div class="review-content">
									<p>
										Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. 
									</p> 
								</div><!-- /.review-content -->
							</li>
							<li>
								<div class="review-metadata">
									<div class="name">
										Peter Tufan : <span>April 3, 2016</span>
									</div>
									<div class="queue">
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
									</div>
								</div><!-- /.review-metadata -->
								<div class="review-content">
									<p>
										Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. 
									</p> 
								</div><!-- /.review-content -->
							</li>
							<li>
								<div class="review-metadata">
									<div class="name">
										Jon Tufan : <span>April 3, 2016</span>
									</div>
									<div class="queue">
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
									</div>
								</div><!-- /.review-metadata -->
								<div class="review-content">
									<p>
										Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. 
									</p> 
								</div><!-- /.review-content -->
							</li>
						</ul><!-- /.review-list -->
					</div><!-- /.col-md-12 -->
				</div><!-- /.row -->
			</div><!-- /.container -->
		</section><!-- /.flat-product-content -->

	 

 
    <style>
            .product .btn-product.btn-add-cart {
                line-height: 40px;
                background: #f48b00;
            }
            .product .btn-product.btn-add-cart img {
                padding-right: 15px;
            }
        </style>
          
@endsection



