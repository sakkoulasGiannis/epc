@extends('epc.layouts.app')


@section('content')
  
<ul class="">
    @foreach($errors->all() as $error)
        <li>{{$error}}</li>
    @endforeach
</ul>
    

@if(count($cartItems) > 0)
    <div id="frontApp" class="mt-5 pt-5 mb-5 pt-5">        
        <checkout-form 
            :cartitems='{{json_encode($cartItems)}}'
            :couriers='{{json_encode($couriers)}}'
            :payments='{{json_encode($payments)}}'
            :cartdetails='{{json_encode($cartDetails)}}'
            :conditions='{{json_encode($conditions)}}'
        ></checkout-form>
    </div>
@else
    <h2>Δέν υπάρχουν προϊόντα στο καλάθι σας!</h2>
@endif

 
@endsection

@section('custom_scripts')
    <script src="/js/frontend/app_front.js"></script>
@endsection