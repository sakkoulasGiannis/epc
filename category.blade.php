@extends('epc.layouts.app')
@section("content")
 



<section class="flat-breadcrumb">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                {{-- <ul class="breadcrumbs">
                    <li class="trail-item">
                        <a href="#" title="">Home</a>
                        <span><img src="images/icons/arrow-right.png" alt=""></span>
                    </li>
                    <li class="trail-item">
                        <a href="#" title="">Shop</a>
                        <span><img src="images/icons/arrow-right.png" alt=""></span>
                    </li>
                    <li class="trail-end">
                        <a href="#" title="">Smartphones</a>
                    </li>
                </ul><!-- /.breacrumbs --> --}}
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.flat-breadcrumb -->

<main id="shop">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-lg-3">
                <div class="sidebar ">
                    <div class="widget widget-categories">
                        <div class="widget-title">
                            <h3>Κατηγορίες<span></span></h3>
                        </div>
                        <ul class="cat-list style1 widget-content">
                            @foreach($category->children as $cat)
                            <li>
                            <a href="./{{$category->slug}}/{{$cat->slug}}"><span>{{$cat->title}}</span></a>
                            </li>
                            @endforeach
                        </ul><!-- /.cat-list -->
                    </div><!-- /.widget-categories -->
                  
                    @include('epc.elements.filters')  
                </div><!-- /.sidebar -->
            </div><!-- /.col-md-4 col-lg-3 -->
            <div class="col-md-8 col-lg-9">
                <div class="main-shop">
                    {{$category->description}}
                    <div class="wrap-imagebox">
                        <div class="flat-row-title style1">
                                <h1>{{$category->title}}</h1>
                             
                            <div class="clearfix"></div>
                        </div>
                        <div class="sort-product">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="showed">
                                        <form action="" method="POST">
                                            @csrf
                                            {!! Form::select('pagination', $pagination, session()->get('paginate'), ['onchange'=>"this.form.submit()",  'name'=>'pagination', 'class'=>'form-control']) !!}
                                        </form>
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    {{$products->links('epc.vendor.pagination.bootstrap-4')}}
                                </div>
                            </div>
                            
                            <div class="clearfix"></div>
                        </div><!-- /.sort-product -->
                        <div class="tab-product">
                            
                            <div class="row sort-box">
                                    @foreach($products as $product)

                                    <div class="col-lg-4 col-sm-6">
                                        <div class="product-box">
                                            <div class="imagebox style8">
                                                {{-- {{\Carbon::now()->diffInWeeks($product->created_at)}} --}}
                                                @if(1 <= 2)
                                                    {{-- <span class="item-new">NEW</span> --}}
                                                @endif
                                                
                                                <div class=" ">
                                                    @if($product->getMedia("product")->first())
                                                        <a href="/{{$product->slug}}" class="">
                                                            @if(count($product->getMedia("product")) > 1)
                                                                @if(count($product->getMedia("product")) > 1)
                                                                    <img class="" src="{{$product->getMedia("product")[($product->categories->where('parent_id','=','3')->first())?0:1]->getUrl("thumb")}}">
                                                                @else
                                                                <img class="" src=" {{$product->getMedia("product")->first()->getUrl("thumb")}}">
                                                                @endif
                                                            
                                                            @else
                                                                <img class=" " src="{{asset($product->getMedia("product")->first()->getUrl("thumb"))}}">
                                                            @endif
                                                        </a>
                            
                                                        @else
                                                    <img class=" " src="{{asset('assets/theme/marron/images/no-image.png')}}">
                                                    @endif
                                                    
                                                </div><!-- /.box-image -->
                                                <div class="box-content">
                                                    
                                                    <div class="product-name">
                                                        <a href="/{{$product->slug}}" title="">{{$product->name}}<br />{{$product->sku}}</a>
                                                    </div>
                                                    <div class="price">
                                                            @if($product->price && $product->originalPrice)
                                                                <del  class="regular  "> {{$product->originalPrice}}€</del>
                                                                <span class="sale  "> {{$product->price}}€</span>
                                                            @else
                                                                <span class="sale">{{$product->price}}€</span>
                                                            @endif                   
                                                        
                                                    </div>
                                                </div><!-- /.box-content -->
                                                {{-- <div class="box-bottom">
                                                    <div class="btn-add-cart">
                                                        <a href="#" title="">
                                                        <img src="{{asset('views/epc/assets/images/icons/add-cart.png')}}" alt="">Add to Cart
                                                        </a>
                                                    </div>
                                                
                                                </div><!-- /.box-bottom --> --}}
                                            </div><!-- /.imagebox style8 -->
                                        </div><!-- /.product-box -->
                                    </div><!-- /.col-md-4 col-sm-6 -->
    
                                @endforeach
    
                               
                                
                            </div><!-- /.sort-box -->
                        </div><!-- /.tab-product -->
                    </div><!-- /.wrap-imagebox -->
                    <div class="blog-pagination">
                            {{$products->links('epc.vendor.pagination.bootstrap-4')}}
                        <div class="clearfix"></div>
                    </div><!-- /.blog-pagination -->
                </div><!-- /.main-shop -->
            </div><!-- /.col-md-8 col-lg-9 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</main><!-- /#shop -->
 
@endsection