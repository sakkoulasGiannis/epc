
@extends('epc.layouts.app')


@section('content')
<div class="container mt-5">
    <div class="row">
        <div class="col-md-12">
            <h1>Συνδεθείτε ή Δημιουργείστε ένα Λογαριασμό</h1>
        </div>
        <div class="col-md-6 ">
            <div class="panel panel-default">
                <h2>Είσοδος</h2>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Να με θυμάσε
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-default">
                                    Είσοδος
                                </button>
                                <br/>
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                        Υπενθύμιση Κωδικού?
                                </a>
                              
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <hr>

        </div>

        <div class="col-md-6">
            <div class="panel panel-default">
                <h2 class="panel-heading">Νέα Εγγραφή</h2>
                <p>Δημιουργώντας ένα λογαριασμό στο κατάστημά μας, θα έχετε τη δυνατότητα να κινηθείτε γρηγορότερα κατά τη διαδικασία πληρωμής, να εισάγετε πολλαπλές διευθύνσεις αποστολής, να βλέπετε και να παρακολουθείτε τις παραγγελίες σας και πολλά άλλα. </p>
                <div class="panel-body">
                    <form  method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="row">
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} col-md-6">
                                <label for="name" class=" control-label">Όνομα</label>
    
                                
                                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
    
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                
                            </div>
        
                            <div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }} col-md-6">
                                <label for="name" class=" control-label">Επίθετο</label>
    
                                
                                    <input id="name" type="text" class="form-control" name="lastname" value="{{ old('lastname') }}" required autofocus>
    
                                    @if ($errors->has('lastname'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('lastname') }}</strong>
                                        </span>
                                    @endif
                                
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }} col-md-6">
                                <label for="mobile" class="control-label">Τηλέφωνο</label>
                                <input id="mobile" type="text" class="form-control" name="mobile" value="{{ old('mobile') }}" required autofocus>
    
                                @if ($errors->has('mobile'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </span>
                                @endif
                            </div>
    
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} col-md-6">
                                <label for="email" class=" control-label">E-Mail Address</label>
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
    
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                       <div class="row">
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} col-md-6">
                                <label for="password" class=" control-label">Κωδικός</label>
                                <input id="password" type="password" class="form-control" name="password" required>
    
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                                
                            </div>
    
                            <div class="form-group col-md-6">
                                <label for="password-confirm" class=" control-label">Επιβεβαίωση Κωδικού</label>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                       </div>
                       <button type="submit" class="btn btn-default">
                            Εγγραφή
                        </button>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
