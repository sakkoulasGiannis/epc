@extends('epc.layouts.app')
@section("content")
    <div id="wrapper" class="category">
        <div class="container-fluid mt-5">
            <div class="row">
                <div class="col-md-12"> 
                    <div class="text-center">
                        <h1>Αναζήτηση</h1>
                    </div>
                     
                </div>
                <div class="col-md-6 offset-md-3">
                    <div class="text-center">
                        <p>Αποτελέσματα για:  {{request()->get('q')}}</p>
                    </div>
                </div>
            </div>
        </div>
          
        
        @include('./epc/elements/products_grid')

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 col-sm-6">
            
    
                    {{$products->links('epc.vendor.pagination.bootstrap-4')}}
                </div>
            </div>
        </div>
        
        
 
    </div>
    

@endsection

@section("custom_scripts")
    <script>
        $("#menu-toggle").click(function (e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });
    </script>

@endsection


