@extends('epc.layouts.app')

@section("content")

   <div class="container mt-5">
       <div class="row">


          <div class="col-md-12">
              <h1>{{__("frontend.fail_checkout")}}</h1>
                
          <p>{!!$order->comment!!}</p>
          </div> 

          <div class="col-md-12">
            @if(isset($info))
            {!!$info!!}
            @endif
          </div>
          

       </div>
   </div>

    @endsection
