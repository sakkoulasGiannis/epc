<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->
<head>
	<!-- Basic Page Needs -->
	<meta charset="UTF-8">
	<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
	<title>@yield('title')</title>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="author" content="CreativeLayers">

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Boostrap style -->
	<link rel="stylesheet" type="text/css" href="{{asset('views/epc/assets/stylesheets/bootstrap.min.css')}}">

	<!-- Theme style -->
	<link rel="stylesheet" type="text/css" href="{{asset('views/epc/assets/stylesheets/style.css')}}">

	<!-- Reponsive -->
	<link rel="stylesheet" type="text/css" href="{{asset('views/epc/assets/stylesheets/responsive.css')}}">

	<link rel="shortcut icon" href="{{asset('views/epc/assets/favicon/favicon.png')}}">

</head>

<body class="header_sticky">
	<div class="boxed">

		 
		 
		<section id="header" class="header">
			<div class="header-top">
				<div class="container">
					<div class="row">
						<div class="col-md-4">
							<ul class="flat-support">
								<li>
									<a href="#" title="">ΠΑΡΑΔΟΣΗ ΣΕ ΟΛΗ ΤΗΝ ΕΛΛΑΔΑ</a>
								</li>
								<li>
									<a href="#" title="">ΔΩΡΕΑΝ ΠΑΡΑΛΑΒΗ</a>
								</li>
								<li>
									<a href="#" title=""></a>
								</li>
							</ul><!-- /.flat-support -->
						</div><!-- /.col-md-4 -->
						<div class="col-md-4">
							<ul class="flat-infomation">
								<li class="phone">
									Επικοινωνήστε Μαζί μας: <a href="#" title="">2811 758 180</a>
								</li>
							</ul><!-- /.flat-infomation -->
						</div><!-- /.col-md-4 -->
						<div class="col-md-4">
							<ul class="flat-unstyled">
								<li class="account">
									<a href="#" title="">Ο ΛΟΓΑΡΙΑΣΜΟΣ ΜΟΥ<i class="fa fa-angle-down" aria-hidden="true"></i></a>
									<ul class="unstyled">
										<li>
											<a href="{{route('login')}}" title="">Είσοδος</a>
										</li>
										{{-- <li>
											<a href="" title="">Wishlist</a>
										</li> --}}
										
										<li>
											<a href="#" title="">Οι Παραγγελίες μου</a>
										</li>
										<li>
											<a href="/cart/" title="">Καλάθι</a>
										</li>
									</ul><!-- /.unstyled -->
								</li>
								 
								 
							</ul><!-- /.flat-unstyled -->
						</div><!-- /.col-md-4 -->
					</div><!-- /.row -->
				</div><!-- /.container -->
			</div><!-- /.header-top -->
			<div class="header-middle">
				<div class="container">
					<div class="row">
						<div class="col-md-3">
							<div id="logo" class="logo">
								<a href="/" title=" home ">
										<img src="{{asset('views/epc/assets/images/ergastirio-logo.jpg')}}" alt="" width="120">
								</a>
							</div><!-- /#logo -->
						</div><!-- /.col-md-3 -->
						<div class="col-md-6">
							<div class="top-search">
									
								<form action="{{route('search')}}" method="get" class="form-search" accept-charset="utf-8">
										{{-- <label for="search">Φόρμα Αναζήτησης</label>
										<input type="text" autocomplete="off" name="q" class="form-control" id="searchform"
											aria-describedby="emailHelp" placeholder=""> --}}

											<div class="box-search">
													<input type="text" autocomplete="off"  name="q" placeholder="">
													<span class="btn-search">
														<button type="submit" class="waves-effect"><img src="{{asset('views/epc/assets/images/icons/search.png')}}" alt=""></button>
													</span>
													 
												</div>
											
								</form><!-- /.form-search -->
							</div><!-- /.top-search -->
						</div><!-- /.col-md-6 -->
						<div class="col-md-3">
							@include('epc/elements/basket')
						</div><!-- /.col-md-3 -->
					</div><!-- /.row -->
				</div><!-- /.container -->
			</div><!-- /.header-middle -->
			<div class="header-bottom">
				<div class="container">
					<div class="row">
						<div class="col-md-3 col-2">
							<div id="mega-menu">
								<div class="btn-mega"><span></span>ΚΑΤΗΓΟΡΙΕΣ</div>
								<ul class="menu">
										@each('epc.elements.categories.childCategories', $categories, 'category')

									<li>
										<a href="#" title="" class="dropdown">
											<span class="menu-img">
												<img src="{{asset('views/epc/assets/images/icons/menu/01.png')}}" alt="">
											</span>
											<span class="menu-title">
												Laptops & Mac
											</span>
										</a>
										<div class="drop-menu">
											<div class="one-third">
												<div class="cat-title">
													Laptop And Mac
												</div>
												<ul>
													<li>
														<a href="#" title="">Networking & Internet Devices</a>
													</li>
													<li>
														<a href="#" title="">Laptops, Desktops & Monitors</a>
													</li>
													<li>
														<a href="#" title="">Hard Drives & Memory Cards</a>
													</li>
													<li>
														<a href="#" title="">Computer Accessories</a>
													</li>
													<li>
														<a href="#" title="">Software</a>
													</li>
												</ul>
												<div class="show">
													<a href="#" title="">Shop All</a>
												</div>
											</div>
											<div class="one-third">
												<div class="cat-title">
													Audio & Video
												</div>
												<ul>
													<li>
														<a href="#" title="">Headphones & Speakers</a>
													</li>
													<li>
														<a href="#" title="">Home Entertainment Systems</a>
													</li>
													<li>
														<a href="#" title="">MP3 & Media Players</a>
													</li>
													<li>
														<a href="#" title="">Software</a>
													</li>
												</ul>
												<div class="show">
													<a href="#" title="">Shop All</a>
												</div>
											</div>
											<div class="one-third">
												<ul class="banner">
													<li>
														<div class="banner-text">
															<div class="banner-title">
																Headphones
															</div>
															<div class="more-link">
																<a href="#" title="">Shop Now <img src="{{asset('views/epc/assets/images/icons/right-2.png" alt')}}=""></a>
															</div>
														</div>
														<div class="banner-img">
															<img src="{{asset('views/epc/assets/images/banner_boxes/menu-01.png')}}" alt="">
														</div>
														<div class="clearfix"></div>
													</li>
													<li>
														<div class="banner-text">
															<div class="banner-title">
																TV & Audio
															</div>
															<div class="more-link">
																<a href="#" title="">Shop Now <img src="{{asset('views/epc/assets/images/icons/right-2.png" alt')}}=""></a>
															</div>
														</div>
														<div class="banner-img">
															<img src="{{asset('views/epc/assets/images/banner_boxes/menu-02.png')}}" alt="">
														</div>
														<div class="clearfix"></div>
													</li>
													<li>
														<div class="banner-text">
															<div class="banner-title">
																Computers
															</div>
															<div class="more-link">
																<a href="#" title="">Shop Now <img src="{{asset('views/epc/assets/images/icons/right-2.png" alt')}}=""></a>
															</div>
														</div>
														<div class="banner-img">
															<img src="{{asset('views/epc/assets/images/banner_boxes/menu-03.png')}}" alt="">
														</div>
														<div class="clearfix"></div>
													</li>
												</ul>	
											</div>
										</div>
									</li>
									 
								</ul>
							</div>
						</div><!-- /.col-md-3 -->
						<div class="col-md-9 col-10">
							<div class="nav-wrap">
								<div id="mainnav" class="mainnav">
									<ul class="menu">
										<li class="column-1">
											<a href="/" title="">Home</a>
											 
										</li><!-- /.column-1 -->
										 
									</ul><!-- /.menu -->
								</div><!-- /.mainnav -->
							</div><!-- /.nav-wrap -->
							<div class="today-deal">
								<a href="#" title="">ΤΕΧΝΙΚΗ ΥΠΟΣΤΗΡΙΞΗ</a>
							</div><!-- /.today-deal -->
							<div class="btn-menu">
	                            <span></span>
	                        </div><!-- //mobile menu button -->
						</div><!-- /.col-md-9 -->
					</div><!-- /.row -->
				</div><!-- /.container -->
			</div><!-- /.header-bottom -->
        </section><!-- /#header -->
        
		@yield('content')
 
		<footer>
				<div class="container">
					<div class="row">
						<div class="col-lg-3 col-md-6">
							<div class="widget-ft widget-about">
								<div class="logo logo-ft">
									<a href="index.html" title="">

											<img src="{{asset('views/epc/assets/images/ergastirio-logo.jpg')}}" alt="" width="120">
									</a>
								</div><!-- /.logo-ft -->
								<div class="widget-content">
									<div class="icon">
										<img src="{{asset('views/epc/assets/images/icons/call.png')}}" alt="">
									</div>
									<div class="info">
										<p class="questions">Έχετε ερωτήσεις;</p>
										<p class="phone">2811 758 180</p>
										<p class="address">
											Κονδυλάκη 46 Ηρακλειο Κρητης ΤΚ: 71305
										</p>
									</div>
								</div><!-- /.widget-content -->
								<ul class="social-list">
									<li>
										<a href="https://www.facebook.com/ergastiriopc/" target="_blank" title="">
											<i class="fa fa-facebook" aria-hidden="true"></i>
										</a>
									</li>
								
								</ul><!-- /.social-list -->
							</div><!-- /.widget-about -->
						</div><!-- /.col-lg-3 col-md-6 -->
						<div class="col-lg-3 col-md-6">
							<div class="widget-ft widget-categories-ft">
								<div class="widget-title">
									<h3>Find By Categories</h3>
								</div>
								<ul class="cat-list-ft">
									<li>
										<a href="#" title="">Desktops</a>
									</li>
									<li>
										<a href="#" title="">Laptops & Notebooks</a>
									</li>
									<li>
										<a href="#" title="">Components</a>
									</li>
									<li>
										<a href="#" title="">Tablets</a>
									</li>
									<li>
										<a href="#" title="">Software</a>
									</li>
									<li>
										<a href="#" title="">Phones & PDAs</a>
									</li>
									<li>
										<a href="#" title="">Cameras</a>
									</li>
								</ul><!-- /.cat-list-ft -->
							</div><!-- /.widget-categories-ft -->
						</div><!-- /.col-lg-3 col-md-6 -->
						<div class="col-lg-2 col-md-6">
							<div class="widget-ft widget-menu">
								<div class="widget-title">
									<h3>Customer Care</h3>
								</div>
								<ul>
									<li>
										<a href="#" title="">
											Contact us
										</a>
									</li>
									<li>
										<a href="#" title="">
											Site Map
										</a>
									</li>
									<li>
										<a href="#" title="">
											My Account
										</a>
									</li>
									<li>
										<a href="#" title="">
											Wish List
										</a>
									</li>
									<li>
										<a href="#" title="">
											Delivery Information
										</a>
									</li>
									<li>
										<a href="#" title="">
											Privacy Policy
										</a>
									</li>
									<li>
										<a href="#" title="">
											Terms & Conditions
										</a>
									</li>
								</ul>
							</div><!-- /.widget-menu -->
						</div><!-- /.col-lg-2 col-md-6 -->
						<div class="col-lg-4 col-md-6">
							<div class="widget-ft widget-newsletter">
								<div class="widget-title">
									<h3>Εγγραφή στο  NewsLetter</h3>
								</div>
								<p>Make sure that you never miss our interesting <br />
									news by joining our newsletter program
								</p>
								<form action="#" class="subscribe-form" method="get" accept-charset="utf-8">
									<div class="subscribe-content">
										<input type="text" name="email" class="subscribe-email" placeholder="Your E-Mail">
										<button type="submit"><img src="{{asset('views/epc/assets/images/icons/right-2.png')}}" alt=""></button>
									</div>
								</form><!-- /.subscribe-form -->
								<ul class="pay-list">
									<li>
										<a href="#" title="">
											<img src="{{asset('views/epc/assets/images/logos/ft-01.png')}}" alt="">
										</a>
									</li>
									<li>
										<a href="#" title="">
											<img src="{{asset('views/epc/assets/images/logos/ft-02.png')}}" alt="">
										</a>
									</li>
									<li>
										<a href="#" title="">
											<img src="{{asset('views/epc/assets/images/logos/ft-03.png')}}" alt="">
										</a>
									</li>
									<li>
										<a href="#" title="">
											<img src="{{asset('views/epc/assets/images/logos/ft-04.png')}}" alt="">
										</a>
									</li>
									<li>
										<a href="#" title="">
											<img src="{{asset('views/epc/assets/images/logos/ft-05.png')}}" alt="">
										</a>
									</li>
								</ul><!-- /.pay-list -->
							</div><!-- /.widget-newsletter -->
						</div><!-- /.col-lg-4 col-md-6 -->
					</div><!-- /.row -->
					
				</div><!-- /.container -->
			</footer><!-- /footer -->
		<section class="footer-bottom">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<p class="copyright"> All Rights Reserved © epc</p>
						<p class="btn-scroll">
							<a href="#" title="">
								<img src="{{asset('views/epc/assets/images/icons/top.png')}}" alt="">
							</a>
						</p>
					</div><!-- /.col-md-12 -->
				</div><!-- /.row -->
			</div><!-- /.container -->
		</section><!-- /.footer-bottom -->

	</div><!-- /.boxed -->

		<!-- Javascript -->
		<script type="text/javascript" src="{{ asset('views/epc/assets/javascript/jquery.min.js')}}"></script>
		<script type="text/javascript" src="{{ asset('views/epc/assets/javascript/tether.min.js')}}"></script>
		<script type="text/javascript" src="{{ asset('views/epc/assets/javascript/bootstrap.min.js')}}"></script>
		<script type="text/javascript" src="{{ asset('views/epc/assets/javascript/waypoints.min.js')}}"></script>
		<!-- <script type="text/javascript" src="{{ asset('views/epc/assets/javascript/jquery.circlechart.js')}}"></script> -->
		<script type="text/javascript" src="{{ asset('views/epc/assets/javascript/easing.js')}}"></script>
<script type="text/javascript" src="{{ asset('views/epc/assets/javascript/jquery.zoom.min.js')}}"></script>
		<script type="text/javascript" src="{{ asset('views/epc/assets/javascript/jquery.flexslider-min.js')}}"></script>
		<script type="text/javascript" src="{{ asset('views/epc/assets/javascript/owl.carousel.js')}}"></script>
		
		<!-- <script type="text/javascript" src="{{ asset('views/epc/assets/javascript/jquery-ui.js')}}"></script> -->
		<script type="text/javascript" src="{{ asset('views/epc/assets/javascript/jquery.mCustomScrollbar.js')}}"></script>
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBtRmXKclfDp20TvfQnpgXSDPjut14x5wk&region=GB"></script>
	   	<script type="text/javascript" src="{{ asset('views/epc/assets/javascript/gmap3.min.js')}}"></script>
	   	<script type="text/javascript" src="{{ asset('views/epc/assets/javascript/waves.min.js')}}"></script> 
		<script type="text/javascript" src="{{ asset('views/epc/assets/javascript/jquery.countdown.js')}}"></script>

		<script type="text/javascript" src="{{ asset('views/epc/assets/javascript/main.js')}}"></script>
		@yield('custom_scripts')

		<style>
		#mega-menu > ul.menu > li > a {
    height: 43px;
    line-height: 50px;
    padding: 0 24px;
    border-bottom: 1px solid #e5e5e5;
    display: block;
    position: relative;
}
</style>
</body>	
</html>