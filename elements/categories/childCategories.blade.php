
<li>
    <a href="/{{(isset($parentCategory))?$parentCategory.'/':null}}{{$category->slug}}"" title="" class="dropdown">
        <span class="menu-img">
            <img src="{{asset('views/epc/assets/images/icons/menu/01.png')}}" alt="">
        </span>
        <span class="menu-title">
                {{$category->title}}
        </span>
    </a>


<div class="drop-menu">
    @if($category->children->count() > 0 )
        @foreach($category->childs as $val => $subcategory)
        <div class="one-third">
                <div class="cat-title">
                   <a href="/{{$category->slug}}/{{$subcategory->slug}}"> {{$subcategory->title}} </a>
                </div>
                <ul>
                    @foreach($subcategory->childs as $sscategory)
                    <li>
                            <a href="/{{$category->slug}}/{{$subcategory->slug}}/{{$sscategory->slug}}"> {{$sscategory->title}} </a>
                        
                    </li>
                    @endforeach
                </ul>
               
            </div>
            
        @endforeach
    @endif
        
        </div>
 
    </li>