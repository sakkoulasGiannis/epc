<?php  $cartContent = \Cart::getContent(); ?>
<div class="box-cart">
        <div class="inner-box">
            <ul class="menu-compare-wishlist">
                {{-- <li class="compare">
                    <a href="compare.html" title="">
                        <img src="{{asset('views/epc/assets/images/icons/compare.png')}}" alt="">
                    </a>
                </li> --}}
                {{-- <li class="wishlist">
                    <a href="wishlist.html" title="">
                        <img src="{{asset('views/epc/assets/images/icons/wishlist.png')}}" alt="">
                    </a>
                </li> --}}
            </ul><!-- /.menu-compare-wishlist -->
        </div><!-- /.inner-box -->
        
        <div class="inner-box">
            <a href="#" title="">
                <div class="icon-cart">
                    <img src="{{asset('views/epc/assets/images/icons/cart.png')}}" alt="">
                    <span>{{$cartContent->count()}}</span>
                </div>
                <div class="price">
                    {{\Cart::getTotal()}}€
                </div>
            </a>
            @if($cartContent->count() > 0 )
            <div class="dropdown-box">
                <ul>
                    @foreach( $cartContent as $val => $item)
                        <li>
                            <div class="img-product">
                                <img src="{{$item->attributes->img_url}}" alt="">
                            </div>
                            <div class="info-product">
                                <div class="name">
                                        {{$item->name}}
                                </div>
                                <div class="price">
                                    
                                    <span>{{$item->getPriceSumWithConditions()}}€</span>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <span class="delete">x</span>
                        </li>
                    @endforeach
                </ul>
                <div class="total">
                    <span>Σύνολο:</span>
                    <span class="price">{{\Cart::getTotal()}}</span>
                </div>
                <div class="btn-cart">
                    <a class="view-cart" href="{{route('cart.checkout')}}">Συνέχεια στο Καλάθι</a>                
                </div>
            </div>
            @endif
        </div><!-- /.inner-box -->
        
    </div><!-- /.box-cart -->