<div class="row">

        @foreach($products as $product)

    <div class="col-md-4 col-sm-6">
        <div class="product-box">
            <div class="imagebox style8">
                {{-- {{\Carbon::now()->diffInWeeks($product->created_at)}} --}}
                @if(1 <= 2)
                <span class="item-new">NEW</span>
                @endif
                
                <div class=" ">
                    @if($product->getMedia("product")->first())
                        <a href="/{{$product->slug}}" class="">
                            @if(count($product->getMedia("product")) > 1)
                                @if(count($product->getMedia("product")) > 1)
                                    <img class="" src="{{$product->getMedia("product")[($product->categories->where('parent_id','=','3')->first())?0:1]->getUrl("thumb")}}">
                                @else
                                <img class="" src=" {{$product->getMedia("product")->first()->getUrl("thumb")}}">
                                @endif
                            
                            @else
                                <img class=" " src="{{asset($product->getMedia("product")->first()->getUrl("thumb"))}}">
                            @endif
                        </a>

                        @else
                    <img class=" " src="{{asset('assets/theme/marron/images/no-image.png')}}">
                    @endif
                    
                </div><!-- /.box-image -->
                <div class="box-content">
                    
                    <div class="product-name">
                        <a href="/{{$product->slug}}" title="">{{$product->name}}<br />{{$product->sku}}</a>
                    </div>
                    <div class="price">
                            @if($product->price && $product->originalPrice)
                                <del  class="regular  "> {{$product->originalPrice}}€</del>
                                <span class="sale  "> {{$product->price}}€</span>
                            @else
                                <span class="sale">{{$product->price}}€</span>
                            @endif                   
                        
                    </div>
                </div><!-- /.box-content -->
                <div class="box-bottom">
                    <div class="btn-add-cart">
                        <a href="#" title="">
                        <img src="{{asset('views/epc/assets/images/icons/add-cart.png')}}" alt="">Add to Cart
                        </a>
                    </div>
                   
                </div><!-- /.box-bottom -->
            </div><!-- /.imagebox style8 -->
        </div><!-- /.product-box -->
    </div><!-- /.col-md-4 col-sm-6 -->

    @endforeach

    
</div><!-- /.row -->