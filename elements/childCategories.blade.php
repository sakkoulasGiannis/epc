@if(isset($subshild) && $subshild == true)
    <a class="dropdown-item" href="/{{(isset($parentCategory))?$parentCategory.'/':null}}{{$category->slug}}">
        {{$category->title}}
    </a>
@else
    <li class="nav-item {{($category->childs && count($category->childs) > 0)?'dropdown':null }} ">
        <a  class="nav-link  {{($category->childs  && count($category->childs))?'dropdown-toggle':null}}" 
            href="/{{(isset($parentCategory))?$parentCategory.'/':null}}{{$category->slug}}"
                {{($category->childs  && count($category->childs))?" role=button data-toggle=dropdown aria-haspopup=true aria-expanded=false " : null }}>
            {{$category->title}}
        </a>
        @if(count($category->childs) > 0)
    {{-- <ul class="dropdown-menu" aria-labelledby="navbarDropdown-{{$category->slug}}"> --}}
    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        @foreach($category->childs as $val => $subcategory)
            @include("epc.elements.childCategories", ['category' => $subcategory, 'parentCategory'=>$category->slug, 'subshild'=>true])    
        @endforeach
    </div>
    @endif
  </li>
@endif

    
  