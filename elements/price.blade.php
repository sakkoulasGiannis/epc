@if($product->price && $product->originalPrice)
    <del  class="" style="font-size:15px;"> {{$product->originalPrice}}€</del>
    <span class="sale  "> {{$product->price}}€</span>
@else
    <span class="sale">{{$product->price}}€</span>
@endif   