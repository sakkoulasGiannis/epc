<?php 
$cartContent = \Cart::getContent();
?>
<div class="btn-group dropleft">
        <a href="#" class='nav-link' data-toggle="dropdown"  >
                <i class="material-icons" style="font-size: 39px; margin-top: -10px;">shopping_basket</i>
                <span class="badge badge-pill badge-warning">{{$cartContent->count()}}</span>
        </a>
        @if($cartContent->count() > 0 )
        <div class="dropdown-menu  p-2">
            <ul class="list-unstyled">
                @foreach( $cartContent as $val => $item)
                    <li class="media p-2">
                        <img class="mr-3" src="{{$item->attributes->img_url}}" width="60" alt="image">
                        <div class="media-body">
                            <p class="mt-0 mb-1">{{$item->name}}</p>
                            <p> {{$item->getPriceSumWithConditions()}}€ </p>
                        </div>
                    </li>
                    <div class="dropdown-divider"></div>
                @endforeach
            </ul>
            <a class="btn btn-default btn-block" href="{{route('cart.checkout')}}">Συνέχεια στο Καλάθι</a>
        </div>
        @endif
</div>