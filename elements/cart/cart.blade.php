<div class="container mt-5 pt-5 mb-5 pb-5 ">
    <div class="row">
        <div class="col-md-12">
            @if(Session::has('message'))
                <p class="alert alert-danger">{{ Session::get('message') }}</p>
            @endif
            
            <section class="flat-shop-cart">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="flat-row-title style1">
                                <h1>Καλάθι Αγορών</h1>
                            </div>
                            <div class="table-cart">
                                <table>
                                    <thead>
                                        <tr>
                                            <th>ΠΡΟΪΟΝ</th>
                                            <th>ΤΜΧ</th>
                                            <th>ΤΙΜΗ</th>
                                            <th> </th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                            @foreach($items as $val => $item)
                                            <tr>
                                                    <td>
                                                        <div class="img-product">
                                                            <img src="{{$item->attributes->img_url}}" width="45" alt="">
                                                        </div>
                                                        <div class="name-product">
                                                            {{$item->name}} <br />{{$item->sku}}
                                                        </div>
                                                        <div class="price">
                                                            {{$item->price}}€
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </td>
                                                    <td>
                                                        <div class="quanlity">
                                                                {{$item->quantity}}
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="total">
                                                                {{$item->getPriceSum()}}€
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <a href="#" title="">
                                                                {!! Form::open(['route' => ['cart.deleteItem', $item->id], 'method' => 'post']) !!}
                                                                <button class="unstyled trunsparent"><img src="{{asset('views/epc/assets/images/icons/delete.png')}}" alt=""></button>
                                                            {!! Form::close() !!}
                                                            
                                                        </a>

                                                    </td>
                                                </tr>
                                        @endforeach

                                        <tfoot>
                                                @if(count($conditions)> 0)
                                                        
                                                    @foreach($conditions as $condition)
                        
                                                        <tr>
                                                            
                                                            <td>{{$condition->getType()}}</td>
                                                            <td>{{$condition->getName()}}</td>
                                                            <td>{{$condition->getValue()}}</td>
                                                            
                                                            <td><a href="{{route('cart.remove.voucher', $condition->getName())}}"><i class="fa fa-trash"></a></td>
                                                        </tr>
                                                    @endforeach
                                                    @endif
                        
                                            <tr>
                                                
                                                <td>ΤΕΛΙΚΟ ΣΥΝΟΛΟ</td>
                                                <td>{{$cartDetails['total']}}€</td>
                                                <td>{{$cartDetails['total_quantity']}}</td>
                                                <td></td>
                                            </tr>
                                            </tfoot>
                                        
                                    </tbody>
                                </table>

                            </div><!-- /.table-cart -->
                        </div><!-- /.col-lg-12 -->                        
                    </div><!-- /.row -->
                    <div class="row">
                            <div class="col-md-6">
                                    <div class="form-coupon">
                                            <form action="#" method="get" accept-charset="utf-8">
                                                <div class="coupon-input">
                                                        {!! Form::open(['route' => 'cart.add.voucher', 'method' => 'post']) !!}
                                                            {!! Form::text('code', null, ['class' => 'form-control', 'placeholder'=>"Κωδικός Κουπονιού"]) !!}      
                                                            <button type="submit">Εφαρμογή</button>
                                                    {!! Form::close() !!}
                                                    
                                                </div>
                                            </form>
                                    </div><!-- /.form-coupon -->
                            </div>
                            <div class="col-md-6">
                                <div class="text-right">
                                    <div class="btn-cart-totals">
                                        <a class="checkout btn btn-orange" href="{{route('cart.checkout')}}">ΟΛΟΚΛΗΡΩΣΗ  </a>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div><!-- /.container -->
            </section><!-- /.flat-shop-cart -->
        </div>
    </div>
</div>

<style>
   button.unstyled.trunsparent {
        background: transparent;
        border: none;
    }
</style>