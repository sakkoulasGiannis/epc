 
<form id='filterForm' ref="form" method="GET" action="{{url()->current()}}">
   

    @foreach($fields as $field) 
    <div class="widget widget-brands">
            <div class="widget-title">
                <h3>{{$field->title}}<span></span></h3>
            </div>
            <div class="widget-content" style="display: none;">
                {{-- <form action="#" method="get" accept-charset="utf-8">
                    <input type="text" name="brands" placeholder="Αναζήτηση {{$field->title}}">
                </form> --}}
                <ul class="box-checkbox scroll">
                    @foreach ($field->values as $value)
                        <li class="check-box">
                            <input type="checkbox" id="{{$value['name']}}" {{(request()->has($value['name']))?'checked':''}} name="brand[][{{$value['name']}}]">
                            <label for="{{$value['name']}}">{{$value['title']}}</label>
                            
                        </li>
                    @endforeach
               
                </ul>
            </div>
        </div><!-- /.widget widget-brands -->
    @endforeach

        
            <button class="btn btn-success btn-block"><img src="images/icons/add-cart.png" alt=""> ΑΝΑΖΗΤΗΣΗ</button>
        
        
    </form>
    
     <style>
     .btn.btn-success{
        line-height: 42px!important;
     }
     .product-box .imagebox{
         min-height: 0; 
     }
     </style>