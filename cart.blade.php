@extends('epc.layouts.app')

@section("content")
    @if(count($items) < 1)
    <div class="container-fluid h-100">
        <div class="row justify-content-center h-100">

            <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8">
                <div class="text-center h100 m-5">
                    <h2>No Items In Cart</h2>
                </div>
            </div>
        </div>
    </div>
        
    @else
        @include("epc.elements.cart.cart")
    @endif

@endsection
 